#ifndef VARIABLES_H
#define VARIABLES_H

// 
#define ROBOT_NUM 14
#define WIRELESS_MODE 1
#define LOCAL_MODE 2
#define RAMP 254

//Number of nodes
#define V 19


//Define Cardinal Points - used to work out which way to turn
#define NORTH 1
#define EAST 2
#define SOUTH 3
#define WEST 4

//Define Targets for collection and delivery - used to choose which microswitches to read
#define WALL 0
#define CUP 1

//Status of robot alignment
#define STRAIGHT 0
#define OFF_LEFT 1
#define OFF_RIGHT 2

//Variables for control of the left motor
#define L_MOTOR MOTOR_1_GO
#define L_FORWARD 127
#define L_REVERSE 255
#define L_MOTOR_INCREMENT 0.3*127

//Variables for control of the right motor
#define R_MOTOR MOTOR_2_GO
#define R_FORWARD 255
#define R_REVERSE 127
#define R_MOTOR_INCREMENT 0.4*127

//Adjacency Matrix
int graph[V][V] = {{0,175,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,125},
{175,0,730,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
{0,730,0,133,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
{0,0,133,0,140,0,0,0,0,850,0,0,0,0,0,0,0,0,0},
{0,0,0,140,0,132,0,0,0,0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,132,0,133,0,0,0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,133,0,132,0,0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,132,0,360,0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,360,0,0,0,0,0,0,0,0,0,0,0},
{0,0,0,850,0,0,0,0,0,0,143,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,143,0,720,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0,720,0,285,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0,0,285,0,180,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0,0,0,180,0,183,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0,0,0,0,183,0,380,0,0,0},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,380,0,720,0,0},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,720,0,143,0},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,143,0,720},
{125,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,720,0}};

//Direction from node to node
int face_matrix[V][V] = {{0,EAST,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NORTH},
{WEST,0,EAST,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
{0,WEST,0,EAST,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
{0,0,WEST,0,EAST,0,0,0,0,NORTH,0,0,0,0,0,0,0,0,0},
{0,0,0,WEST,0,EAST,0,0,0,0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,WEST,0,EAST,0,0,0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,WEST,0,EAST,0,0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,WEST,0,NORTH,0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,SOUTH,0,0,0,0,0,0,0,0,0,0,0},
{0,0,0,SOUTH,0,0,0,0,0,0,NORTH,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,SOUTH,0,NORTH,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0,SOUTH,0,WEST,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0,0,EAST,0,WEST,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0,0,0,EAST,0,WEST,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0,0,0,0,EAST,0,WEST,0,0,0},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,EAST,0,SOUTH,0,0},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NORTH,0,SOUTH,0},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NORTH,0,SOUTH},
{SOUTH,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,NORTH,0}};

#endif
