stopwatch node_watch;

void follow_line(){
	read_driving_sensors();
	node_watch.start();
	cout << "Driving forward to next node" << endl;
	while(1)
	{
		read_driving_sensors();
		
		static int l_motor_value = L_FORWARD;
		static int r_motor_value = R_FORWARD;
		static int robot_on_line = STRAIGHT;
		
		if(sensor_2_on_line and not sensor_1_on_line and not sensor_3_on_line)
		{
			// Robot is straight
			//~ cout << "Robot is straight" << endl;
			l_motor_value = L_FORWARD;
			r_motor_value = R_FORWARD;
			robot_on_line = STRAIGHT;
		}
		if(sensor_3_on_line and not sensor_1_on_line)
		{
			//~ cout << "Robot is off left" << endl;
			l_motor_value = L_FORWARD;
			r_motor_value = R_FORWARD - R_MOTOR_INCREMENT;
			robot_on_line = OFF_LEFT;
		}
		if(sensor_3_on_line and not sensor_1_on_line and not sensor_2_on_line)
		{
			//~ cout << "Robot is very off left" << endl;
			l_motor_value = L_FORWARD;
			r_motor_value = R_FORWARD - 2*R_MOTOR_INCREMENT;
			robot_on_line = OFF_LEFT;
		}
		if(sensor_1_on_line and not sensor_3_on_line)
		{
			//~ cout << "Robot is off right" << endl;
			r_motor_value = R_FORWARD;
			l_motor_value = L_FORWARD - L_MOTOR_INCREMENT;
			robot_on_line = OFF_RIGHT;
		}
		if(sensor_1_on_line and not sensor_3_on_line and not sensor_2_on_line)
		{
			//~ cout << "Robot is far off right" << endl;
			r_motor_value = R_FORWARD;
			l_motor_value = L_FORWARD - 2*L_MOTOR_INCREMENT;
			robot_on_line = OFF_RIGHT;
		}
		if(not sensor_1_on_line and not sensor_2_on_line and not sensor_3_on_line)
		{
			//~ static int lost_count = 0;
			//~ cout << "Robot is lost - attempt " << lost_count << endl;
			//~ lost_count ++;
			//~ read_driving_sensors();
			//~ if(lost_count > 5 and (not sensor_1_on_line and not sensor_2_on_line and not sensor_3_on_line))
			//~ {
				//~ lost_count = 0;
				cout << "Robot is lost - reset" << endl;
				if(robot_on_line == OFF_LEFT)
				{
					//~ cout << "Reversing (R faster)" << endl;
					l_motor_value = L_REVERSE - L_MOTOR_INCREMENT;
					r_motor_value = R_REVERSE;
				}
				if(robot_on_line == OFF_RIGHT)
				{
					//~ cout << "Reversing (L faster)" << endl;
					l_motor_value = L_REVERSE;
					r_motor_value = R_REVERSE - R_MOTOR_INCREMENT;
				}
				else
				{
					//~ cout << "Reversing (equal speed)" << endl;
					l_motor_value = L_REVERSE;
					r_motor_value = R_REVERSE;
				}
			
				Marvin.motor_control(L_MOTOR, l_motor_value);
				Marvin.motor_control(R_MOTOR, r_motor_value);
				
				//delay(200);
				
				//~ static stopwatch backup;
				//~ backup.start();
				//~ static bool node_crossed = false;
				//~ while(backup.read() < 200){
					//~ read_driving_sensors();
					//~ if(sensor_4_on_line)
					//~ {
						//~ cout << "Node Crossed" << endl;
						//~ node_crossed = true;
					//~ }
				//~ }
				//~ cout << "Done Reversing" << endl;
				//~ if (node_crossed)
				//~ {
					//~ cout << "Node Detected Whilst Reversing" << endl;
					//~ follow_line();
					//~ node_crossed = false;
					//~ cout << "Crossed Back, Continue Driving" << endl;
				//~ }
			//~ }
		}
		if(sensor_4_on_line and (sensor_2_on_line or sensor_1_on_line or sensor_3_on_line))
		{
			// Delay to prevent detect same node twice
			if(node_watch.read() < 200 and node_watch.read() != 0)
			{
				//~ cout << "Node encountered too quickly" << endl;
				continue;
			}
			cout << "Node found" << endl;
			break;
		}			
		//cout << "L: " << l_motor_value << " R: " << r_motor_value << endl;
		Marvin.motor_control(L_MOTOR, l_motor_value);
		Marvin.motor_control(R_MOTOR, r_motor_value);
	}
	Marvin.motor_control(L_MOTOR, L_FORWARD);
	Marvin.motor_control(R_MOTOR, R_FORWARD);
	delay(140);
	Marvin.motor_control(L_MOTOR, 0);
	Marvin.motor_control(R_MOTOR, 0);
	//~ delay(200);
}

void turn_left_90()
{
	// Turn 90 deg
	cout << "Turning left... ";
	// Stop L motor
	//~ Marvin.motor_control(L_MOTOR, L_REVERSE);
	Marvin.motor_control(L_MOTOR, L_REVERSE-L_MOTOR_INCREMENT);
	// Run R motor
	//~ Marvin.motor_control(R_MOTOR, R_FORWARD);
	Marvin.motor_control(R_MOTOR, R_FORWARD-R_MOTOR_INCREMENT);
	// Ignoring crossing back over the centre line
	delay(500);
	// Refresh the sensors
	read_driving_sensors();
	// Continue refreshing sensors until it is recentred on line
	while(not sensor_1_on_line)
	{
		read_driving_sensors();
		//cout << "Reading sensors..." << sensor_1_on_line << endl;
	}	
	cout << "Done" << endl;
	
	Marvin.motor_control(L_MOTOR, 0);
	Marvin.motor_control(R_MOTOR, 0);
	delay(100);	
}

void turn_right_90()
{
	// Turn 90 deg
	cout << "Turning right... ";
	// Run L motor
	Marvin.motor_control(L_MOTOR, L_FORWARD-L_MOTOR_INCREMENT);
	//~ Marvin.motor_control(L_MOTOR, L_FORWARD);
	// Reverse R motor
	Marvin.motor_control(R_MOTOR, R_REVERSE-R_MOTOR_INCREMENT);
	//~ Marvin.motor_control(R_MOTOR, R_REVERSE);
	// Ignoring crossing back over the centre line
	delay(500);
	// Refresh the sensors
	read_driving_sensors();
	// Continue refreshing sensors until it is recentred on line
	while(not sensor_3_on_line)
	{
		read_driving_sensors();
		//cout << "Reading sensors..." << sensor_3_on_line << endl;
	}	
	cout << "Done" << endl;
	
	Marvin.motor_control(L_MOTOR, 0);
	Marvin.motor_control(R_MOTOR, 0);
	delay(100);	
}

void turn_180_node()
{
	cout << "Turning 180... ";
	turn_left_90();
	Marvin.motor_control(L_MOTOR, L_REVERSE);
	Marvin.motor_control(R_MOTOR, R_REVERSE);
	delay(200);
	turn_left_90();
	//~ // Turn 90
	//~ Marvin.motor_control(L_MOTOR, L_FORWARD-L_MOTOR_INCREMENT/2);
	//~ Marvin.motor_control(R_MOTOR, R_REVERSE-R_MOTOR_INCREMENT/2);
	//~ // Ignore crossing back over the centre line
	//~ delay(300);
	//~ // Continue refreshing sensors until it is recentred on line
	//~ read_driving_sensors();
	//~ while(not sensor_3_on_line)
	//~ {
		//~ read_driving_sensors();
		//~ //cout << "Reading sensors..." << sensor_3_on_line << endl;
	//~ }
	//~ // Back up a little bit
	//~ Marvin.motor_control(R_MOTOR, R_REVERSE);
	//~ Marvin.motor_control(L_MOTOR, L_REVERSE);
	//~ delay(500);
	//~ // Turn another 90
	//~ Marvin.motor_control(L_MOTOR, L_FORWARD-L_MOTOR_INCREMENT/2);
	//~ Marvin.motor_control(R_MOTOR, R_REVERSE-R_MOTOR_INCREMENT/2);
	//~ // Ignore crossing back over the centre line
	//~ delay(300);
	//~ // Continue refreshing sensors until it is recentred on line	
	//~ read_driving_sensors();
	//~ while(not sensor_3_on_line)
	//~ {
		//~ read_driving_sensors();
		//cout << "Reading sensors..." << sensor_3_on_line << endl;
	//~ }
	//~ // Allow extra time for recentreing
	//~ cout << "Done" << endl;
}

void turn_180_line()
{
	cout << "Turning 180 on the line" << endl;
	Marvin.motor_control(L_MOTOR, L_FORWARD);
	Marvin.motor_control(R_MOTOR, R_REVERSE);
	delay(500);
	while(1)
	{
		read_driving_sensors();
		if(sensor_2_on_line) break;
	}
	Marvin.motor_control(L_MOTOR, 0);
	Marvin.motor_control(R_MOTOR, 0);
	delay(200);
	cout << "Done" << endl;
}

void reverse_to_line()
{
	// Reverse past all nodes
	cout << "Reversing for 1s" << endl;
	Marvin.motor_control(L_MOTOR, L_REVERSE);
	Marvin.motor_control(R_MOTOR, R_REVERSE);
	delay(1000);
	Marvin.motor_control(L_MOTOR, 0);
	Marvin.motor_control(R_MOTOR, 0);
	delay(200);
	// Go back towards the node
	follow_line();
}



void Drive_to_target(int target){
	stopwatch delivery_watch;
	
	while(1)
	{
		read_driving_sensors();
		static bool left_switch;
		static bool right_switch;
		if (target == CUP){
			left_switch = bottom_left_microswitch;
			right_switch = bottom_right_microswitch;
		}
		else if (target == WALL){
			left_switch = top_left_microswitch;
			right_switch = top_right_microswitch;
		}
				
		static int l_motor_value = L_FORWARD;
		static int r_motor_value = R_FORWARD;
		static int robot_on_line = STRAIGHT;
		
		if(left_switch or right_switch)
		{
			delivery_watch.start();
			while(1)
			{
				if(left_switch and right_switch) break;
				else if(delivery_watch.read() > 2000){
					cout << "Took to long to line up" << endl;
					break;
				}
				else
				{
					Marvin.motor_control(L_MOTOR, L_FORWARD);
					Marvin.motor_control(R_MOTOR, R_FORWARD);
					read_driving_sensors();
					if (target == CUP){
						left_switch = bottom_left_microswitch;
						right_switch = bottom_right_microswitch;
					}
					else if (target == WALL){
						left_switch = top_left_microswitch;
						right_switch = top_right_microswitch;
					}
				}
			}
			break;
		}
		
		if(sensor_2_on_line and not sensor_1_on_line and not sensor_3_on_line)
		{
			// Robot is straight
			//~ cout << "Robot is straight" << endl;
			l_motor_value = L_FORWARD;
			r_motor_value = R_FORWARD;
			robot_on_line = STRAIGHT;
		}
		if(sensor_3_on_line and not sensor_1_on_line)
		{
			//~ cout << "Robot is off left" << endl;
			l_motor_value = L_FORWARD;
			r_motor_value = R_FORWARD - R_MOTOR_INCREMENT;
			robot_on_line = OFF_LEFT;
		}
		if(sensor_3_on_line and not sensor_1_on_line and not sensor_2_on_line)
		{
			//~ cout << "Robot is very off left" << endl;
			l_motor_value = L_FORWARD;
			r_motor_value = R_FORWARD - 2*R_MOTOR_INCREMENT;
			robot_on_line = OFF_LEFT;
		}
		if(sensor_1_on_line and not sensor_3_on_line)
		{
			//~ cout << "Robot is off right" << endl;
			r_motor_value = R_FORWARD;
			l_motor_value = L_FORWARD - L_MOTOR_INCREMENT;
			robot_on_line = OFF_RIGHT;
		}
		if(sensor_1_on_line and not sensor_3_on_line and not sensor_2_on_line)
		{
			//~ cout << "Robot is far off right" << endl;
			r_motor_value = R_FORWARD;
			l_motor_value = L_FORWARD - 2*L_MOTOR_INCREMENT;
			robot_on_line = OFF_RIGHT;
		}
		if(not sensor_1_on_line and not sensor_2_on_line and not sensor_3_on_line)
		{
			//~ cout << "Robot is lost" << endl;
			if(robot_on_line == OFF_LEFT)
			{
				//~ cout << "Reversing (R faster)" << endl;
				l_motor_value = L_REVERSE - L_MOTOR_INCREMENT;
				r_motor_value = R_REVERSE;
			}
			if(robot_on_line == OFF_RIGHT)
			{
				//~ cout << "Reversing (L faster)" << endl;
				l_motor_value = L_REVERSE;
				r_motor_value = R_REVERSE - R_MOTOR_INCREMENT;
			}
			else
			{
				//~ cout << "Reversing (equal speed)" << endl;
				l_motor_value = L_REVERSE;
				r_motor_value = R_REVERSE;
			}
		}
		Marvin.motor_control(L_MOTOR, l_motor_value);
		Marvin.motor_control(R_MOTOR, r_motor_value);
	}
	if (Marvin.Destination == 8){
		Marvin.motor_control(L_MOTOR, 0);
		Marvin.motor_control(R_MOTOR, 0);
		delay(200);
		Marvin.motor_control(L_MOTOR, L_REVERSE);
		Marvin.motor_control(R_MOTOR, R_REVERSE);
		delay(200);
	}
	Marvin.motor_control(L_MOTOR, 0);
	Marvin.motor_control(R_MOTOR, 0);
	delay(200);
}
