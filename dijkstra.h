#include <limits.h>
#include <vector>
using namespace std;

vector<int> shortest_path;

int minDistance(int dist[], bool sptSet[])
{
	int min = INT_MAX, min_index;

	for (int v = 0; v < V; v++)
		if (sptSet[v] == false && dist[v] <= min)
			min = dist[v], min_index = v;

	return min_index;
}

vector<int> Path(int parent[], int dst)
{
    int node;
    shortest_path.clear();

	while (parent[dst]!=-1){
	    node = parent[dst];
	    shortest_path.insert(shortest_path.begin(), dst);
	    dst = node;
	}
	return shortest_path;
}

vector<int> dijkstra(int src, int dst)
{
	
	int dist[V];

	bool sptSet[V];

	int parent[V];

	for (int i = 0; i < V; i++)
	{
		parent[src] = -1;
		dist[i] = INT_MAX;
		sptSet[i] = false;
	}

	dist[src] = 0;

	for (int count = 0; count < V-1; count++)
	{
		int u = minDistance(dist, sptSet);

		sptSet[u] = true;

		for (int v = 0; v < V; v++)

			if (!sptSet[v] && graph[u][v] &&
				dist[u] + graph[u][v] < dist[v])
			{
				parent[v] = u;
				dist[v] = dist[u] + graph[u][v];
			} 
	}
	return Path(parent, dst);
}
