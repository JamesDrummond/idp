#include <iostream>
#include <robot_instr.h>
#include <robot_link.h>
#include <stopwatch.h>
#include <delay.h>
#include <cmath>
#include <stdio.h>
#include "variables.h"
#include "robot_class.h"
#include "sensors.h"
#include "arm_control.h"
#include "dijkstra.h"
#include "driving.h"
#include "navigate.h"
#include "robot.h"
using namespace std;

// Store whether in wireless or local mode
int mode;

void initialise_link()
{
	// Establish a link
	while(1)
	{
	// Local version
	#ifdef __arm__
		mode = LOCAL_MODE;
		if (!Marvin.rlink.initialise("127.0.0.1"))
	// Wireless version
	#else
		mode = WIRELESS_MODE;
		if (!Marvin.rlink.initialise(ROBOT_NUM))
	#endif
		{
			cout << "Cannot initialise link" << endl;
			Marvin.rlink.print_errs("  ");
		}
		else break;
	}
	cout << "Link successfully initialised in mode: " << mode << endl;
}

void test_link()
{
	// Test the link
	stopwatch startup_watch;
	startup_watch.start();
	
	// Send test instruction
	while(1)
	{
		int response = Marvin.rlink.request(TEST_INSTRUCTION);
		
		// Check the response
		if (response == TEST_INSTRUCTION_RESULT)
		{
			cout << "Test passed in " << startup_watch.read() << "ms" << endl;
			break;
		}
		else if (response == REQUEST_ERROR) {
		  cout << "Fatal errors on link:" << endl;
		  Marvin.rlink.print_errs();
		}
		else
		{
		  cout << "Test failed (bad value returned)" << endl;
		}
	}
}

