#define CLAW_MOTOR MOTOR_3_GO
#define CLAW_CLOSE 255
#define CLAW_OPEN 127
#define CLAW_LOCK 200

void Close(){
	Marvin.motor_control(CLAW_MOTOR, CLAW_CLOSE); //Close claw
	read_ball_sensors();
	cout << "Closing Claw" << endl;
	while(!claw_microswitch){ //Until microswitch is pressed
		read_ball_sensors();
	}
	delay(100);
	Marvin.motor_control(CLAW_MOTOR, 0);
	Marvin.motor_control(CLAW_MOTOR, CLAW_LOCK); //Apply a torque to hold jaws shut
	cout << "Closed Claw" << endl;
}

void Open(){
	//Open the claws by a predetermined amount
	cout << "Opening" << endl;
	Marvin.motor_control(CLAW_MOTOR, CLAW_OPEN);
	delay(300);
	Marvin.motor_control(CLAW_MOTOR, 0);
	cout << "Opened Claw" << endl;
}
