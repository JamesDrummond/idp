#include "robot_connect.h"

int main()  
{             
	stopwatch match_timer; 
	  
    initialise_link();
    test_link();
    Marvin.initialise(); 
        
    cout << "Started Successfully" << endl; 
  
    cin.get(); //Wait to start - Means robot will be connected and initialised before the timer starts
    match_timer.start(); 
        
    while (Marvin.Delivered < 6){
		cout << "Balls Delivered: " << Marvin.Delivered << endl;
		Marvin.Destination = Marvin.Delivered + 2;
        Marvin.Path_Find();
        cout << "Arrived to collect ball: " << Marvin.Delivered + 1 << endl;
        Marvin.Locate_Ball();
        Marvin.Pickup();
        Marvin.Return_to_Path();
        Marvin.Identify_Ball();
        Marvin.Path_Find();
        Marvin.Locate_Cup();
        delay(300); 
        Marvin.Deliver_Ball();
        Marvin.Return_to_Path(); 
        cout << "Delivered Ball" << endl;
        cout << "Time: " << match_timer.read() << endl; 
        if (match_timer.read() > 270000 and Marvin.Delivered >= 3){
			cout << "Running out of time" << endl;
			break;}
	}
	cout << "Balls Delivered: " << Marvin.Delivered << endl;
	Marvin.Destination = 0; 
    Marvin.Path_Find();
    if (Marvin.Direction == SOUTH){
		turn_left_90();
	}
    cout << "FINISHED" << endl;
    return 0;
}
