#ifndef ROBOTCLASS_H
#define ROBOTCLASS_H

class Robot {
	public:
		int Delivered; //Number of balls delivered
		int Location; //Node robot was last at
		int Direction; //Direction the robot is facing
		int Carrying; //Type of ball robot is carrying
		int Destination; //Where the robot is going
		
		robot_link rlink; //Set up link to robot
		
		void initialise(void);
		void Path_Find(void);
		void Pickup(void);
		void Identify_Ball(void);
		void Return_to_Path(void);
		void Deliver_Ball(void);
		void Locate_Ball(void);
		void Locate_Cup(void);
		void motor_control(enum command_instruction motor, int speed);
		int sensor_control(enum request_instruction sensor);
}Marvin;
#endif
