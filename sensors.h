#ifndef SENSORS_H
#define SENSORS_H

#define DRIVING_SENSORS READ_PORT_2

/**********************************************/

//Defining which bit corresponds to each Microswitch
#define MICROSWITCH_TOP_LEFT 16
#define MICROSWITCH_TOP_RIGHT 32
#define MICROSWITCH_BOTTOM_LEFT 64
#define MICROSWITCH_BOTTOM_RIGHT 128

bool top_left_microswitch = false;
bool top_right_microswitch = false;
bool bottom_left_microswitch = false;
bool bottom_right_microswitch = false;

/*********************************************/

//Defining which bit corresponds to each line sensor
#define LINE_SENSOR_1 4
#define LINE_SENSOR_2 2
#define LINE_SENSOR_3 1
#define LINE_SENSOR_4 8

bool sensor_1_on_line = false;
bool sensor_2_on_line = false;
bool sensor_3_on_line = false;
bool sensor_4_on_line = false;

/***********************************************/

#define BALL_SENSORS READ_PORT_5

//Defining which bit corresponds to each sensor for detecting balls
#define STRAIN_GAUGE 1
#define LDR_1 2
#define LDR_2 4
#define CLAW_SWITCH 8

bool heavy_ball = false;
bool multi_coloured = false;
bool yellow = false;
bool white = false;
bool claw_microswitch = false;

/***********************************************/

using namespace std;

void read_driving_sensors()
{
	// Read from bus
	int sensor_word = Marvin.sensor_control(DRIVING_SENSORS);
	
	// Check line sensors
	if (sensor_word bitand LINE_SENSOR_1)
	{
		sensor_1_on_line = true;
		//~ cout << "Sensor 1 on line" << endl;
	}
	else
	{
		sensor_1_on_line = false;
	}
	
	if (sensor_word bitand LINE_SENSOR_2)
	{
		sensor_2_on_line = true;
		//~ cout << "Sensor 2 on line" << endl;
	}
	else
	{
		sensor_2_on_line = false;
	}
	
	if (sensor_word bitand LINE_SENSOR_3)
	{
		sensor_3_on_line = true;
		//~ cout << "Sensor 3 on line" << endl;
	}
	else
	{
		sensor_3_on_line = false;
	}
	
	if (sensor_word bitand LINE_SENSOR_4)
	{
		sensor_4_on_line = true;
		//~ cout << "Sensor 4 on line" << endl;
	}
	else
	{
		sensor_4_on_line = false;
	}
	
	// Check microswitches
	if (sensor_word bitand MICROSWITCH_TOP_LEFT)
	{
		top_left_microswitch = true;
		//~ cout << "Top left microswitch pressed" << endl;
	}
	else
	{
		top_left_microswitch = false;
	}
	
	if (sensor_word bitand MICROSWITCH_TOP_RIGHT)
	{
		top_right_microswitch = true;
		//~ cout << "Top right microswitch pressed" << endl;
	}
	else
	{
		top_right_microswitch = false;
	}
	
	if (sensor_word bitand MICROSWITCH_BOTTOM_LEFT)
	{
		bottom_left_microswitch = true;
		//~ cout << "Bottom left microswitch pressed" << endl;
	}
	else
	{
		bottom_left_microswitch = false;
	}
	
	if (sensor_word bitand MICROSWITCH_BOTTOM_RIGHT)
	{
		bottom_right_microswitch = true;
		//~ cout << "Bottom right microswitch pressed" << endl;
	}
	else
	{
		bottom_right_microswitch = false;
	}
}


void read_ball_sensors()
{
	// Read from bus
	int sensor_word = Marvin.sensor_control(BALL_SENSORS);
	//~ cout << sensor_word;
	
	if (not(sensor_word bitand STRAIN_GAUGE))
	{
		heavy_ball = true;
		//~  	cout << "Ball is heavy" << endl;
	}
	else
	{
		heavy_ball = false;
	}
	
	if ((sensor_word bitand LDR_1) and (sensor_word bitand LDR_2))
	{
		yellow = true;
		//~ cout << "Ball is yellow" << endl;
	}
	else
	{
		yellow = false;
	}
	
	if ((sensor_word bitand LDR_1) and not (sensor_word bitand LDR_2))
	{
		white = true;
		//~ cout << "Ball is white" << endl;
	}
	else
	{
		white = false;
	}
	
	if (not (sensor_word bitand LDR_1))
	{
		multi_coloured = true;
		//~ cout << "Ball is multi-coloured" << endl;
	}
	else
	{
		multi_coloured = false;
	}
	if (sensor_word bitand CLAW_SWITCH)
	{
		claw_microswitch = true;
		//~ cout << "Claw is shut" << endl;
	}
	else
	{
		claw_microswitch = false;
	}
}

int Identify_Type(){
	read_ball_sensors();
	int type;
	cout << "###################" << endl;
	if (not heavy_ball and white){
		type = 1;
		cout << "Type: Light White" << endl;
	}
	if (heavy_ball and white){
		type = 2;
		cout << "Type: Heavy White" << endl;
	}
	if (not heavy_ball and yellow){
		type = 3;
		cout << "Type: Light Yellow - Squishy :) " << endl;
	}
	if (heavy_ball and yellow){
		type = 4;
		cout << "Type: Heavy Yellow" << endl;
	}
	if (not heavy_ball and multi_coloured){
		type = 5;
		cout << "Type: Multi" << endl;
	}
	cout << "###################" << endl;
	return type;
}

#endif
