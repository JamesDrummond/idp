
//Work out which way to turn given a target direction
void Turn_direction(int Target){
	int diff = Marvin.Direction - Target;
	if (diff == 0){
		cout << "Correct Direction Already" << endl;
	}
	else {
		if (diff == -1 or diff == 3){
			cout << "Turning Right" << endl;
			turn_right_90();
		}
		else if(diff == 1 or diff == -3){
			cout << "Turning Left" << endl;
			turn_left_90();
		}
		else if (diff == 2 or  diff == -2){
			cout << "Turning About" << endl;
			turn_180_node();
		}
		Marvin.motor_control(L_MOTOR, 0);
		Marvin.motor_control(R_MOTOR, 0);
		delay(100);
		Marvin.Direction = Target; 
	}
}

//Work out the delivery location given the ball type
int Delivery_Location(int Type){
	int destination;
	switch(Type) {
		case 1 :
			destination = 12;
			cout << "Destination: " << destination << " - D1"<< endl;
			break;
		case 3 :
			destination = 13;
			cout << "Destination: " << destination << " - D2"<< endl;
			break;
		case 5 :
			destination = 14;
			cout << "Destination: " << destination << " - D3"<< endl;
			break;
		default :
			destination = 8;
			cout << "Destination: " << destination << " - DR"<< endl;
			break;
   }
   return destination;
}
