#ifndef ROBOT_H
#define ROBOT_H

#include <fstream>

void Robot::initialise() {
	//Initialise robot vairables
	Location = 0;
	Direction = EAST;
	Open(); //Start with claw open
	
	//Read from output file (used in the case of restart)
	static ifstream infile; 
	infile.open("output.dat");
	infile >> Delivered >> Carrying;
	infile.close();
	cout << "Delivered: " << Delivered << ", Ball is type: " << Carrying << endl;
}

void Robot::Path_Find() {
	
    shortest_path = dijkstra(Location, Destination); //Use Dijkstra's Algorithm to find shortest route
    
    //For each edge in shortest path
	for (unsigned int i = 0; i != shortest_path.size(); i++){
		cout << Location << " to " << shortest_path[i] << endl;
        int direction_to_node = face_matrix[Location][shortest_path[i]]; //Which direction should robot face
        cout << "Turning to face " << direction_to_node << endl;
        Turn_direction(direction_to_node); //Turn to face correct direction
        cout << "Done Turning, starting to drive" << endl;
        
        follow_line(); //Drive to next node
        
        Location = shortest_path[i]; //Update location
	}
	rlink.command(L_MOTOR, 0);
	rlink.command(R_MOTOR, 0);
}

void Robot::Locate_Ball(){
	Turn_direction(SOUTH); //Turn to face wall
	cout << "Driving to Wall" << endl;
	Drive_to_target(WALL);
}

void Robot::Pickup(void) {
	Close(); //Close claw
}

void Robot::Identify_Ball(void) {
	if( Carrying == -1 ) { //If ball not already identified
		delay(1000); //Stop for long enough for ball to settle
		cout << "Detecting ball type" << endl;
		Carrying = Identify_Type(); //Identify the type of ball
	}
	
	Destination = Delivery_Location(Carrying); //Work out the delivery location
	
	//Save type of ball to file (to save time on restart)
	ofstream outfile;
	outfile.open("output.dat");
	outfile << Delivered << endl << Carrying;
}

void Robot::Return_to_Path(){
	cout << "Return to Path" << endl;
	// If at DR
	if (Destination == 8 or Destination == 11){
		reverse_to_line();
	}
	// If at D1,2,3
	else if (Destination == 12 or Destination == 13 or Destination == 14)
	{
		Marvin.motor_control(L_MOTOR, L_REVERSE);
		Marvin.motor_control(R_MOTOR, R_REVERSE);
		delay(500);
		// Turn 180
		turn_180_line();
		follow_line();
		Direction = NORTH;
	}
	// If at P
	else{
		reverse_to_line();
	}
}

void Robot::Locate_Cup(){
	//If at DR
	if (Destination == 8){
		Turn_direction(EAST);
		Direction = EAST;
	}
	//If at D1,2,3
	else {
		Turn_direction(SOUTH);
		Direction = SOUTH;
	}
	
	Drive_to_target(CUP);
	delay(400);
}

void Robot::Deliver_Ball() {
	Open(); //Open claw
	Delivered ++; //Increase number of balls delivered
	Carrying = -1; //Not carrying any balls
	
	//Save number of balls delivered and ball being carried to file
	static ofstream outfile;
	outfile.open("output.dat");
	outfile << Delivered << endl << Carrying << endl;
}

/***********************************************/

//Error Handling

void Robot::motor_control(enum command_instruction motor, int speed){
	//Keep trying to send command until successful
	while(1){
		rlink.command(motor, speed);
		if (Marvin.rlink.any_errs()){ //check for errors
			if (rlink.fatal_err()) { // check for fatal errors
				rlink.print_errs(); // print them out
				rlink.reinitialise(); // flush the link
				continue; //Retry sending
			}
			//Check what the error is - if too many errors reinitialise than try again, otherwise just try again
			int err = Marvin.rlink.get_err();
			if (err == OVERFLOW){ 
				rlink.reinitialise();
			}
		}
		else {break;}; //If successful continue running
	}
}

int Robot::sensor_control(enum request_instruction sensor){
	int sensor_word;
	//Keep trying to read sensors until successful
	while(1){
		sensor_word = rlink.request(sensor);
		if (sensor_word != REQUEST_ERROR){ //If no errors continue running
			break;
		}
		//Check what the error is - if too many errors reinitialise than try again, otherwise just try again
		int err = Marvin.rlink.get_err();
		if (err == OVERFLOW){
			rlink.reinitialise();
		}
	}
	return sensor_word;
}
#endif
